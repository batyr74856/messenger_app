import 'package:cloud_firestore/cloud_firestore.dart';

class Message {
  final String sendId;
  final String sendName;
  final String sendSurname;
  final String receiverId;
  final String message;
  final Timestamp time;
  final String color;

  Message(
      {required this.sendId,
      required this.sendName,
      required this.sendSurname,
      required this.receiverId,
      required this.message,
      required this.time,
      required this.color});

  Map<String, dynamic> toMap() {
    return {
      'sendId': sendId,
      'sendName': sendName,
      'sendSurname': sendSurname,
      'receiverId': receiverId,
      'message': message,
      'time': time,
      'color': color
    };
  }
}
