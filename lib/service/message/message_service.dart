import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:messanger_app/model/message_model.dart';

class MessageService extends ChangeNotifier {
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  final FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;

  Future<void> sendMessage(String sendName, String sendSurname,
      String receiverId, String message, String color) async {
    final String currentUserId = firebaseAuth.currentUser?.uid ?? '';
    final Timestamp time = Timestamp.now();

    Message newMessage = Message(
        sendId: currentUserId,
        sendName: sendName,
        sendSurname: sendSurname,
        receiverId: receiverId,
        message: message,
        time: time,
        color: color);
    List<String> ids = [currentUserId, receiverId];
    ids.sort();
    String chatRoomId = ids.join('_');
    print(chatRoomId);
    print(newMessage.toMap());

    await firebaseFirestore
        .collection('chat_rooms')
        .doc(chatRoomId)
        .collection('messages')
        .add(newMessage.toMap());
  }

  Stream<QuerySnapshot> getMessages(String userId, String otherId) {
    List<String> ids = [userId, otherId];
    ids.sort();
    String chatRoomId = ids.join('_');

    return firebaseFirestore
        .collection('chat_rooms')
        .doc(chatRoomId)
        .collection('messages')
        .orderBy('time', descending: false)
        .snapshots();
  }
}
