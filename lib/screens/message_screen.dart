import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:messanger_app/service/message/message_service.dart';

class MessageScreen extends StatefulWidget {
  final String sendName;
  final String sendSurname;
  final String receiverId;
  final String color;
  const MessageScreen(
      {super.key,
      required this.sendName,
      required this.receiverId,
      required this.sendSurname,
      required this.color});

  @override
  State<MessageScreen> createState() => _MessageScreenState();
}

class _MessageScreenState extends State<MessageScreen> {
  final TextEditingController messageEditingController =
      TextEditingController();
  final MessageService messageService = MessageService();
  final firebaseAuth = FirebaseAuth.instance;

  void sendMessage() async {
    if (messageEditingController.text.isNotEmpty) {
      await messageService.sendMessage(widget.sendName, widget.sendSurname,
          widget.receiverId, messageEditingController.text, widget.color);
      messageEditingController.clear();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(255, 255, 255, 1),
      appBar: AppBar(
          backgroundColor: const Color.fromRGBO(255, 255, 255, 1),
          elevation: 0,
          bottom: PreferredSize(
            preferredSize: const Size.fromHeight(25),
            child: Column(
              children: [
                Row(
                  children: [
                    IconButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        color: const Color.fromRGBO(
                          43,
                          51,
                          62,
                          1,
                        ),
                        iconSize: 22,
                        icon: const Icon(Icons.arrow_back_ios)),
                    Container(
                      alignment: Alignment.center,
                      height: 50,
                      width: 50,
                      decoration: BoxDecoration(
                        color: widget.color == 'green'
                            ? const Color.fromRGBO(
                                31,
                                219,
                                95,
                                1,
                              )
                            : widget.color == 'red'
                                ? const Color.fromRGBO(237, 57, 0, 1)
                                : const Color.fromRGBO(0, 109, 237, 1),
                        borderRadius: BorderRadius.circular(50),
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            widget.sendName[0],
                            style: const TextStyle(
                                color: Colors.white,
                                fontFamily: 'Gilroy',
                                fontSize: 20,
                                fontWeight: FontWeight.w700),
                          ),
                          Text(
                            widget.sendSurname[0],
                            style: const TextStyle(
                                color: Colors.white,
                                fontFamily: 'Gilroy',
                                fontSize: 20,
                                fontWeight: FontWeight.w700),
                          )
                        ],
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Text(
                              widget.sendName,
                              style: const TextStyle(
                                  fontFamily: 'Gilroy',
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600),
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            Text(
                              widget.sendSurname,
                              style: const TextStyle(
                                  fontFamily: 'Gilroy',
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          'В сети',
                          style: TextStyle(
                              color: Color.fromRGBO(
                                94,
                                122,
                                144,
                                1,
                              ),
                              fontFamily: 'Gilroy',
                              fontSize: 12,
                              fontWeight: FontWeight.w600),
                        ),
                      ],
                    ),
                  ],
                ),
                const Divider(
                  color: Color.fromRGBO(
                    237,
                    242,
                    246,
                    1,
                  ),
                  thickness: 1.2,
                )
              ],
            ),
          )),
      body: Column(
        children: [
          Expanded(child: buildMessageList()),
          const Divider(
            color: Color.fromRGBO(
              237,
              242,
              246,
              1,
            ),
            thickness: 1,
            height: 6,
          ),
          buildMessageInput()
        ],
      ),
    );
  }

  Widget buildMessageList() {
    return StreamBuilder(
        stream: messageService.getMessages(
            widget.receiverId, firebaseAuth.currentUser?.uid ?? ''),
        builder: ((context, snapshot) {
          if (snapshot.hasError) {
            return const Text('Error');
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: SizedBox(
                width: 15,
                height: 15,
                child: CircularProgressIndicator(
                  color: Color.fromRGBO(
                    80,
                    209,
                    119,
                    1,
                  ),
                ),
              ),
            );
          }
          return ListView(
              children:
                  snapshot.data!.docs.map((e) => buildMessageItem(e)).toList());
        }));
  }

  Widget buildMessageItem(DocumentSnapshot doc) {
    Map<String, dynamic> data = doc.data() as Map<String, dynamic>;
    var alignment = (data['sendId'] == firebaseAuth.currentUser?.uid)
        ? Alignment.centerRight
        : Alignment.centerLeft;
    var color = (data['sendId'] == firebaseAuth.currentUser?.uid)
        ? const Color.fromRGBO(60, 237, 120, 1)
        : const Color.fromRGBO(237, 242, 246, 1);
    var textColor = (data['sendId'] == firebaseAuth.currentUser?.uid)
        ? const Color.fromRGBO(0, 82, 28, 1)
        : const Color.fromRGBO(43, 51, 62, 1);
    String formatTimestamp(Timestamp timestamp) {
      var format = DateFormat('HH:mm');
      return format.format(timestamp.toDate());
    }

    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      alignment: alignment,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 5),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(16),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Flexible(
              child: Text(
                data['message'],
                style: TextStyle(
                    color: textColor,
                    fontSize: 14,
                    fontFamily: 'Gilroy',
                    fontWeight: FontWeight.w500),
              ),
            ),
            SizedBox(
              width: 20,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                  height: 20,
                ),
                Text(
                  formatTimestamp(data['time']),
                  style: TextStyle(
                      color: textColor,
                      fontSize: 12,
                      fontFamily: 'Gilroy',
                      fontWeight: FontWeight.w500),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget buildMessageInput() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
      child: Row(
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
            decoration: BoxDecoration(
              color: const Color.fromRGBO(237, 242, 246, 1),
              borderRadius: BorderRadius.circular(12),
            ),
            child: const Icon(
              Icons.attach_file,
              size: 20,
            ),
          ),
          const SizedBox(
            width: 10,
          ),
          Expanded(
              child: TextField(
            controller: messageEditingController,
            decoration: const InputDecoration(
                hintText: 'Сообщение',
                contentPadding:
                    EdgeInsets.symmetric(vertical: 8, horizontal: 15),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.all(Radius.circular(12)),
                ),
                hintStyle: TextStyle(fontFamily: 'Gilroy', fontSize: 16),
                border: OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.all(Radius.circular(12)),
                ),
                filled: true,
                fillColor: Color.fromRGBO(237, 242, 246, 1)),
          )),
          const SizedBox(
            width: 10,
          ),
          InkWell(
            onTap: sendMessage,
            child: Container(
                alignment: Alignment.center,
                padding: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(237, 242, 246, 1),
                  borderRadius: BorderRadius.circular(12),
                ),
                child: const Icon(
                  Icons.send,
                  size: 20,
                )),
          ),
        ],
      ),
    );
  }
}
