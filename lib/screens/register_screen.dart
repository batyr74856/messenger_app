import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:messanger_app/screens/login_screen.dart';
import 'package:messanger_app/service/auth/auth_state.dart';
import 'package:messanger_app/service/auth/auth_service.dart';
import 'package:provider/provider.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

bool isLoading = false;
bool _obscureText = true;
bool _obscurePassword = true;

class _RegisterPageState extends State<RegisterPage> {
  TextEditingController emailTextInputController = TextEditingController();
  TextEditingController passwordTextInputController = TextEditingController();
  TextEditingController repeatPasswordTextInputController =
      TextEditingController();
  TextEditingController nameTextInputController = TextEditingController();
  TextEditingController surnameTextInputController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  @override
  void dispose() {
    emailTextInputController.dispose();
    passwordTextInputController.dispose();
    nameTextInputController.dispose();
    repeatPasswordTextInputController.dispose();
    surnameTextInputController.dispose();

    super.dispose();
  }

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  void _togglePassword() {
    setState(() {
      _obscurePassword = !_obscurePassword;
    });
  }

  void signUp() async {
    if (passwordTextInputController.text !=
        repeatPasswordTextInputController.text) {
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text('Неодинаковые пароли')));
      return;
    }
    final authService = Provider.of<AuthService>(context, listen: false);
    try {
      await authService.signUpWithEmailAndPassword(
          emailTextInputController.text,
          passwordTextInputController.text,
          nameTextInputController.text,
          surnameTextInputController.text);
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Отлично! Регистрация прошла успешно')));
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => const AuthState()),
          (Route<dynamic> route) => false);
    } catch (e) {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text(e.toString())));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(
        255,
        255,
        255,
        1,
      ),
      appBar: null,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Form(
            key: formKey,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.03,
                  ),
                  Row(
                    children: [
                      Text(
                        "*Обязательные поля",
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 10,
                          fontWeight: FontWeight.w400,
                          color: Colors.red,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      Text(
                        "Почта*",
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          color: Colors.grey[400],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Stack(
                    children: [
                      Material(
                        elevation: 1,
                        shadowColor: Colors.grey,
                        borderRadius: BorderRadius.circular(12),
                        child: Container(
                          height: 50,
                          width: double.infinity,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(12),
                          ),
                        ),
                      ),
                      TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        autocorrect: false,
                        controller: emailTextInputController,
                        validator: (email) =>
                            email != null && !EmailValidator.validate(email)
                                ? 'Введите правильный Email'
                                : null,
                        style: const TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.w400,
                            color: Color.fromRGBO(
                              42,
                              43,
                              55,
                              1,
                            ),
                            fontSize: 14),
                        textAlign: TextAlign.left,
                        decoration: InputDecoration(
                          isCollapsed: true,
                          errorStyle:
                              const TextStyle(fontSize: 11, height: 0.1),
                          contentPadding: const EdgeInsets.symmetric(
                              vertical: 18, horizontal: 12),
                          border: InputBorder.none,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.018,
                  ),
                  Row(
                    children: [
                      Text(
                        "Имя*",
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          color: Colors.grey[400],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Stack(
                    children: [
                      Material(
                        elevation: 1,
                        shadowColor: Colors.grey,
                        borderRadius: BorderRadius.circular(12),
                        child: Container(
                          height: 50,
                          width: double.infinity,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(12),
                          ),
                        ),
                      ),
                      TextFormField(
                        keyboardType: TextInputType.name,
                        autocorrect: false,
                        controller: nameTextInputController,
                        validator: (name) => name == null && name == ''
                            ? 'Имя не может быть пустым'
                            : null,
                        style: const TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.w400,
                            color: Color.fromRGBO(
                              42,
                              43,
                              55,
                              1,
                            ),
                            fontSize: 14),
                        textAlign: TextAlign.left,
                        decoration: InputDecoration(
                          isCollapsed: true,
                          errorStyle:
                              const TextStyle(fontSize: 11, height: 0.1),
                          contentPadding: const EdgeInsets.symmetric(
                              vertical: 18, horizontal: 12),
                          border: InputBorder.none,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.018,
                  ),
                  Row(
                    children: [
                      Text(
                        "Фамилия",
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          color: Colors.grey[400],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Stack(
                    children: [
                      Material(
                        elevation: 1,
                        shadowColor: Colors.grey,
                        borderRadius: BorderRadius.circular(12),
                        child: Container(
                          height: 50,
                          width: double.infinity,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(12),
                          ),
                        ),
                      ),
                      TextFormField(
                        keyboardType: TextInputType.name,
                        autocorrect: false,
                        controller: surnameTextInputController,
                        style: const TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.w400,
                            color: Color.fromRGBO(
                              42,
                              43,
                              55,
                              1,
                            ),
                            fontSize: 14),
                        textAlign: TextAlign.left,
                        decoration: InputDecoration(
                          isCollapsed: true,
                          errorStyle:
                              const TextStyle(fontSize: 11, height: 0.1),
                          contentPadding: const EdgeInsets.symmetric(
                              vertical: 18, horizontal: 12),
                          border: InputBorder.none,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.018,
                  ),
                  Row(
                    children: [
                      Text(
                        "Пароль*",
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          color: Colors.grey[400],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Stack(
                    children: [
                      Material(
                        elevation: 1,
                        shadowColor: Colors.grey,
                        borderRadius: BorderRadius.circular(12),
                        child: Container(
                          width: double.infinity,
                          height: 50,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(12),
                          ),
                        ),
                      ),
                      TextFormField(
                        obscureText: _obscurePassword,
                        autocorrect: false,
                        controller: passwordTextInputController,
                        validator: (value) => value != null && value.length < 2
                            ? 'В пароле должно быть минимум 2 символа'
                            : null,
                        style: const TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.w400,
                            color: Color.fromRGBO(
                              42,
                              43,
                              55,
                              1,
                            ),
                            fontSize: 14),
                        textAlign: TextAlign.left,
                        decoration: InputDecoration(
                          isCollapsed: true,
                          errorStyle:
                              const TextStyle(fontSize: 11, height: 0.1),
                          suffix: GestureDetector(
                              onTap: () {
                                _togglePassword();
                              },
                              child: _obscurePassword
                                  ? Icon(
                                      Icons.visibility_off_outlined,
                                      color: Colors.grey,
                                      size: 22,
                                    )
                                  : Icon(
                                      Icons.visibility_outlined,
                                      color: Colors.green,
                                      size: 22,
                                    )),
                          contentPadding: const EdgeInsets.symmetric(
                              vertical: 14, horizontal: 12),
                          border: InputBorder.none,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.018,
                  ),
                  Row(
                    children: [
                      Text(
                        "Повторите пароль*",
                        style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          color: Colors.grey[400],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Stack(
                    children: [
                      Material(
                        elevation: 1,
                        shadowColor: Colors.grey,
                        borderRadius: BorderRadius.circular(12),
                        child: Container(
                          width: double.infinity,
                          height: 50,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(12),
                          ),
                        ),
                      ),
                      TextFormField(
                        obscureText: _obscureText,
                        autocorrect: false,
                        controller: repeatPasswordTextInputController,
                        validator: (value) => value != null && value.length < 2
                            ? 'В пароле должно быть минимум 2 символа'
                            : null,
                        style: const TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.w400,
                            color: Color.fromRGBO(
                              42,
                              43,
                              55,
                              1,
                            ),
                            fontSize: 14),
                        textAlign: TextAlign.left,
                        decoration: InputDecoration(
                          isCollapsed: true,
                          errorStyle:
                              const TextStyle(fontSize: 11, height: 0.1),
                          suffix: GestureDetector(
                              onTap: () {
                                _toggle();
                              },
                              child: _obscureText
                                  ? Icon(
                                      Icons.visibility_off_outlined,
                                      color: Colors.grey,
                                      size: 22,
                                    )
                                  : Icon(
                                      Icons.visibility_outlined,
                                      color: Colors.green,
                                      size: 22,
                                    )),
                          contentPadding: const EdgeInsets.symmetric(
                              vertical: 14, horizontal: 12),
                          border: InputBorder.none,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.045,
                  ),
                  InkWell(
                    onTap: signUp,
                    child: SizedBox(
                      width: double.infinity,
                      height: 45,
                      child: Material(
                        borderRadius: BorderRadius.circular(12),
                        shadowColor: Colors.greenAccent,
                        color: const Color.fromRGBO(
                          80,
                          209,
                          119,
                          1,
                        ),
                        elevation: 5.0,
                        child: isLoading
                            ? const Center(
                                child: SizedBox(
                                  width: 15,
                                  height: 15,
                                  child: CircularProgressIndicator(
                                    color: Colors.white,
                                  ),
                                ),
                              )
                            : const Center(
                                child: Text(
                                  'Регистрация',
                                  style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontSize: 14.0,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.015,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => LoginScreen()),
                              ModalRoute.withName("/LoginScreen"));
                        },
                        child: Row(
                          children: const [
                            Text(
                              "Есть аккаунт?",
                              style: TextStyle(
                                fontSize: 11,
                                fontWeight: FontWeight.w300,
                                fontFamily: 'Montserrat',
                                color: Color.fromRGBO(
                                  42,
                                  43,
                                  55,
                                  1,
                                ),
                              ),
                            ),
                            SizedBox(width: 5),
                            Text(
                              "Войти",
                              style: TextStyle(
                                  fontSize: 11,
                                  fontWeight: FontWeight.w300,
                                  fontFamily: 'Montserrat',
                                  color: Color.fromRGBO(
                                    42,
                                    43,
                                    55,
                                    1,
                                  ),
                                  decoration: TextDecoration.underline),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 15),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
