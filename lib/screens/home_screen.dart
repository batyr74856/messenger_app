import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:messanger_app/screens/message_screen.dart';
import 'package:messanger_app/service/auth/auth_service.dart';
import 'package:provider/provider.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  void signOut() async {
    final authService = Provider.of<AuthService>(context, listen: false);
    return await authService.signOut();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color.fromRGBO(255, 255, 255, 1),
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(255, 255, 255, 1),
          elevation: 0,
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(90),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text(
                        'Чаты',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontFamily: 'Gilroy',
                            color: Color.fromRGBO(43, 51, 62, 1),
                            fontSize: 32,
                            fontWeight: FontWeight.w600),
                      ),
                      IconButton(
                        icon: const Icon(Icons.login_outlined),
                        onPressed: signOut,
                      )
                    ],
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: TextField(
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.search),
                        hintText: 'Поиск',
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 8, horizontal: 15),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide.none,
                          borderRadius: BorderRadius.all(Radius.circular(12)),
                        ),
                        hintStyle:
                            TextStyle(fontFamily: 'Gilroy', fontSize: 16),
                        border: OutlineInputBorder(
                          borderSide: BorderSide.none,
                          borderRadius: BorderRadius.all(Radius.circular(12)),
                        ),
                        filled: true,
                        fillColor: Color.fromRGBO(237, 242, 246, 1)),
                  ),
                ),
                const SizedBox(
                  height: 15,
                ),
                const Divider(
                  color: Color.fromRGBO(
                    237,
                    242,
                    246,
                    1,
                  ),
                  thickness: 1.2,
                )
              ],
            ),
          ),
        ),
        body: buildUserList());
  }

  final FirebaseAuth auth = FirebaseAuth.instance;

  Widget buildUserList() {
    return StreamBuilder<QuerySnapshot>(
        stream: FirebaseFirestore.instance.collection('users').snapshots(),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return const Text('error');
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: SizedBox(
                width: 15,
                height: 15,
                child: CircularProgressIndicator(
                  color: Color.fromRGBO(
                    80,
                    209,
                    119,
                    1,
                  ),
                ),
              ),
            );
          }
          return ListView(
            children: snapshot.data!.docs
                .map<Widget>((doc) => buildUserListItem(doc))
                .toList(),
          );
        });
  }

  Widget buildUserListItem(DocumentSnapshot doc) {
    Map<String, dynamic> data = doc.data()! as Map<String, dynamic>;

    if (auth.currentUser?.uid != data['uid']) {
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          children: [
            const SizedBox(
              height: 12,
            ),
            ListTile(
              leading: Container(
                alignment: Alignment.center,
                height: 50,
                width: 50,
                decoration: BoxDecoration(
                  color: data['color'] == 'green'
                      ? const Color.fromRGBO(
                          31,
                          219,
                          95,
                          1,
                        )
                      : data['color'] == 'red'
                          ? const Color.fromRGBO(237, 57, 0, 1)
                          : const Color.fromRGBO(0, 109, 237, 1),
                  borderRadius: BorderRadius.circular(50),
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      data['name'][0],
                      style: const TextStyle(
                          color: Colors.white,
                          fontFamily: 'Gilroy',
                          fontSize: 20,
                          fontWeight: FontWeight.w700),
                    ),
                    Text(
                      data['surname'][0],
                      style: const TextStyle(
                          color: Colors.white,
                          fontFamily: 'Gilroy',
                          fontSize: 20,
                          fontWeight: FontWeight.w700),
                    )
                  ],
                ),
              ),
              title: Row(
                children: [
                  Text(
                    data['name'],
                    style: const TextStyle(
                        fontFamily: 'Gilroy',
                        fontSize: 15,
                        fontWeight: FontWeight.w600),
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  Text(
                    data['surname'],
                    style: const TextStyle(
                        fontFamily: 'Gilroy',
                        fontSize: 15,
                        fontWeight: FontWeight.w600),
                  )
                ],
              ),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: ((context) => MessageScreen(
                            sendName: data['name'],
                            receiverId: data['uid'],
                            sendSurname: data['surname'],
                            color: data['color']))));
              },
            ),
            SizedBox(
              height: 12,
            ),
            Divider(
              color: Color.fromRGBO(
                237,
                242,
                246,
                1,
              ),
              thickness: 1.2,
            )
          ],
        ),
      );
    } else {
      return Container();
    }
  }
}
