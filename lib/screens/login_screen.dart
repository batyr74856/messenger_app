import 'package:flutter/material.dart';
import 'package:email_validator/email_validator.dart';
import 'package:messanger_app/screens/register_screen.dart';
import 'package:messanger_app/service/auth/auth_service.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool isLoading = false;
  bool _obscurePassword = true;

  TextEditingController emailTextInputController = TextEditingController();
  TextEditingController passwordTextInputController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  @override
  void dispose() {
    emailTextInputController.dispose();
    passwordTextInputController.dispose();

    super.dispose();
  }

  void signIn() async {
    final authService = Provider.of<AuthService>(context, listen: false);
    try {
      await authService.signInWithEmailAndPassword(
          emailTextInputController.text, passwordTextInputController.text);
    } catch (e) {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text(e.toString())));
    }
  }

  @override
  Widget build(BuildContext context) {
    void _togglePassword() {
      setState(() {
        _obscurePassword = !_obscurePassword;
      });
    }

    return Scaffold(
        backgroundColor: const Color.fromRGBO(
          255,
          255,
          255,
          1,
        ),
        appBar: null,
        body: SafeArea(
          child: SingleChildScrollView(
            child: Form(
              key: formKey,
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
                child: Column(
                  children: [
                    const BuildWelcomeText(),
                    Row(
                      children: [
                        Text(
                          "Почта",
                          style: TextStyle(
                            fontFamily: 'Gilroy',
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                            color: Colors.grey[400],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Stack(
                      children: [
                        Material(
                          elevation: 1,
                          shadowColor: Colors.grey,
                          borderRadius: BorderRadius.circular(12),
                          child: Container(
                            height: 50,
                            width: double.infinity,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(12),
                            ),
                          ),
                        ),
                        TextFormField(
                          keyboardType: TextInputType.emailAddress,
                          autocorrect: false,
                          controller: emailTextInputController,
                          validator: (email) =>
                              email != null && !EmailValidator.validate(email)
                                  ? 'Введите правильный Email'
                                  : null,
                          style: const TextStyle(
                              fontFamily: 'Gilroy',
                              fontWeight: FontWeight.w400,
                              color: Color.fromRGBO(
                                42,
                                43,
                                55,
                                1,
                              ),
                              fontSize: 14),
                          textAlign: TextAlign.left,
                          decoration: InputDecoration(
                            isCollapsed: true,
                            errorStyle:
                                const TextStyle(fontSize: 11, height: 0.1),
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 18, horizontal: 12),
                            border: InputBorder.none,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.02,
                    ),
                    Row(
                      children: [
                        Text(
                          "Пароль",
                          style: TextStyle(
                            fontFamily: 'Gilroy',
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                            color: Colors.grey[400],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Stack(
                      children: [
                        Material(
                          elevation: 1,
                          shadowColor: Colors.grey,
                          borderRadius: BorderRadius.circular(12),
                          child: Container(
                            width: double.infinity,
                            height: 50,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(12),
                            ),
                          ),
                        ),
                        TextFormField(
                          obscureText: _obscurePassword,
                          autocorrect: false,
                          controller: passwordTextInputController,
                          validator: (value) =>
                              value != null && value.length < 2
                                  ? 'В пароле должно быть минимум 2 символа'
                                  : null,
                          style: const TextStyle(
                              fontFamily: 'Gilroy',
                              fontWeight: FontWeight.w400,
                              color: Color.fromRGBO(
                                42,
                                43,
                                55,
                                1,
                              ),
                              fontSize: 14),
                          textAlign: TextAlign.left,
                          decoration: InputDecoration(
                            isCollapsed: true,
                            errorStyle:
                                const TextStyle(fontSize: 11, height: 0.1),
                            suffix: GestureDetector(
                                onTap: () {
                                  _togglePassword();
                                },
                                child: _obscurePassword
                                    ? Icon(
                                        Icons.visibility_off_outlined,
                                        color: Colors.grey,
                                        size: 22,
                                      )
                                    : Icon(
                                        Icons.visibility_outlined,
                                        color: Colors.green,
                                        size: 22,
                                      )),
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 14, horizontal: 12),
                            border: InputBorder.none,
                          ),
                        ),
                      ],
                    ),
                    Container(
                      padding: const EdgeInsets.only(
                        top: 10,
                        left: 167,
                      ),
                      child: GestureDetector(
                        child: const Text(
                          "Забыли пароль?",
                          style: TextStyle(
                              fontSize: 10,
                              fontWeight: FontWeight.w300,
                              fontFamily: 'Gilroy',
                              color: Color.fromRGBO(
                                42,
                                43,
                                55,
                                1,
                              ),
                              decoration: TextDecoration.underline),
                        ),
                        onTap: () {},
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.045,
                    ),
                    GestureDetector(
                      onTap: signIn,
                      child: SizedBox(
                        width: double.infinity,
                        height: 50,
                        child: Material(
                          borderRadius: BorderRadius.circular(12),
                          shadowColor: Colors.greenAccent,
                          color: const Color.fromRGBO(
                            80,
                            209,
                            119,
                            1,
                          ),
                          elevation: 5.0,
                          child: isLoading
                              ? const Center(
                                  child: SizedBox(
                                    width: 15,
                                    height: 15,
                                    child: CircularProgressIndicator(
                                      color: Colors.white,
                                    ),
                                  ),
                                )
                              : const Center(
                                  child: Text(
                                    'Войти',
                                    style: TextStyle(
                                      fontFamily: 'Gilroy',
                                      fontSize: 14.0,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.015,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        const RegisterPage()));
                          },
                          child: const Row(
                            children: [
                              Text(
                                "Еще нет аккаунта?",
                                style: TextStyle(
                                  fontSize: 11,
                                  fontWeight: FontWeight.w300,
                                  fontFamily: 'Gilroy',
                                  color: Color.fromRGBO(
                                    42,
                                    43,
                                    55,
                                    1,
                                  ),
                                ),
                              ),
                              SizedBox(width: 5),
                              Text(
                                "Зарегистрироваться",
                                style: TextStyle(
                                    fontSize: 11,
                                    fontWeight: FontWeight.w300,
                                    fontFamily: 'Gilroy',
                                    color: Color.fromRGBO(
                                      42,
                                      43,
                                      55,
                                      1,
                                    ),
                                    decoration: TextDecoration.underline),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}

class BuildWelcomeText extends StatelessWidget {
  const BuildWelcomeText({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.05,
        ),
        const Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Добро пожаловать!",
              style: TextStyle(
                fontFamily: 'Gilroy',
                color: Color.fromRGBO(
                  42,
                  43,
                  55,
                  1,
                ),
                fontSize: 18.0,
                fontWeight: FontWeight.w200,
              ),
            ),
          ],
        ),
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.1,
        ),
      ],
    );
  }
}
